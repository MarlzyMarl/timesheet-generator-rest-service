﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using TimeSheetAPI.Models;
using TimeSheetAPI.Helpers;

namespace TimeSheetAPI.Controllers
{
    public class TimeSheetController : ApiController
    {
        [EnableCors(origins: "http://localhost:3000", headers: "*", methods: "POST")]
        public TimeSheetResponse JSONData(TimeSheetRequest timesheetRq)
        {
            ValidationHelper.validateTimesheetRq(timesheetRq);
            List<int> timesheetDays = new List<int>();

            if(timesheetRq.placementType.ToString() == nameof(enumerators.PlacementType.Weekly))
            {
                timesheetDays = CalculateWeeks.getWeeklyTimesheetDays(timesheetRq.placementStartDate, timesheetRq.placementEndDate);
            }
            else
            {
                timesheetDays = CalculateMonths.getMonthlyTimesheetDays(timesheetRq.placementStartDate, timesheetRq.placementEndDate);
            }

            TimeSheetResponse timesheetRs = CreateTimeSheetResponse.createTimeSheetResponse(timesheetRq, timesheetDays);
            
            return timesheetRs;
        }
    }
}
