﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TimeSheetAPI.Models;

namespace TimeSheetAPI.Helpers
{
    public static class CreateTimeSheetResponse
    {
        public static TimeSheetResponse createTimeSheetResponse(TimeSheetRequest timesheetRq, List<int> listOfDays)
        {
            return new TimeSheetResponse()
            {
                candidateName = timesheetRq.candidateName,
                clientName = timesheetRq.clientName,
                jobTitle = timesheetRq.jobTitle,
                placementStartDate = timesheetRq.placementStartDate,
                placementEndDate = timesheetRq.placementEndDate,
                placementType = nameof(timesheetRq.placementType),
                days = listOfDays.ToArray()
            };
        }
    }
}