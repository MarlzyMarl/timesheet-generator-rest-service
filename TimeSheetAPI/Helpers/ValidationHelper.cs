﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TimeSheetAPI.Models;

namespace TimeSheetAPI.Helpers
{
    public static class ValidationHelper
    {
        public static void validateTimesheetRq(TimeSheetRequest timesheetRq)
        {
            DateTime startDate;
            DateTime endDate;

            if (string.IsNullOrWhiteSpace(timesheetRq.candidateName)){
                throw new ArgumentNullException("Candidate Name is Empty");
            }

            if (string.IsNullOrWhiteSpace(timesheetRq.clientName))
            {
                throw new ArgumentNullException("Client Name is Empty");
            }

            if (string.IsNullOrWhiteSpace(timesheetRq.candidateName))
            {
                throw new ArgumentNullException("Job Title is Empty");
            }

            try
            {
                if(DateTime.Parse(timesheetRq.placementStartDate) > DateTime.Parse(timesheetRq.placementEndDate))
                {
                    throw new InvalidOperationException("Start day is not less than end date");
                }
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }

            if (timesheetRq.placementType.ToString() != "Weekly" && timesheetRq.placementType.ToString() != "Monthly")
            {
                throw new InvalidOperationException("Placement type is incorrect");
            }

        }
    }
}