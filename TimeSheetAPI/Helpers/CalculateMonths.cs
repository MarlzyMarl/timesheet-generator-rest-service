﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TimeSheetAPI.Helpers
{
    public static class CalculateMonths
    {
        public static List<int> getMonthlyTimesheetDays(string startDate, string endDate)
        {
            List<int> daysInMonth = new List<int>();
            DateTime startDay = DateTime.Parse(startDate);
            DateTime endDay = DateTime.Parse(endDate);
            int currentYear = startDay.Year;
            int currentMonth = startDay.Month + 1;

            int amountOfMonths = (endDay.Month + endDay.Year * 12) - (startDay.Month + startDay.Year * 12);
            if(amountOfMonths == 0)
            {
                daysInMonth.Add(((int)endDay.Day - (int)startDay.Day) + 1);
            }

            if(amountOfMonths > 0)
            {
                daysInMonth.Add((DateTime.DaysInMonth(startDay.Year, startDay.Month) - (int)startDay.Day+ 1));

                for (int i = 0; i < amountOfMonths-1; i++)
                {
                    daysInMonth.Add(DateTime.DaysInMonth(startDay.Year, currentMonth));
                    currentMonth++;
                    if(currentMonth > 12)
                    {
                        currentYear++;
                        currentMonth = 1;
                    }
                }

                daysInMonth.Add(endDay.Day);
            }

            return daysInMonth;
        }
    }
}