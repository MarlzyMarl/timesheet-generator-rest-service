﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TimeSheetAPI.Helpers
{
    public static class CalculateWeeks
    {
        public static List<int> getWeeklyTimesheetDays(string startDate, string endDate)
        {
            int defaultDaysInWeek = 7;
            List<int> daysInWeek = new List<int>();
            DateTime startDay = Convert.ToDateTime(startDate);
            DateTime endDay = Convert.ToDateTime(endDate);
            DateTime nextSunday = startDay.AddDays(7 - (int)startDay.DayOfWeek);

            DateTime lastSunday = endDay.AddDays(7 - (int)endDay.DayOfWeek).AddDays(-7);

            var weeks = ((lastSunday - nextSunday).TotalDays / 7);

            if (startDay.DayOfWeek.ToString() == "Sunday")
            {
                daysInWeek.Add(1);
                daysInWeek.Add((int)(nextSunday - startDay).TotalDays);
            }
            else if (weeks < 0)
            {
                daysInWeek.Add((int)(endDay - startDay).TotalDays + 1);
            }
            else
            {
                daysInWeek.Add((int)(nextSunday - startDay).TotalDays + 1);
            }

            for (int i = 0; i < weeks; i++)
            {
                daysInWeek.Add(defaultDaysInWeek);
            }

            if (lastSunday < endDay && !(weeks < 0))
            {
                daysInWeek.Add((int)endDay.DayOfWeek);
            }

            return daysInWeek;
        }
    }
}