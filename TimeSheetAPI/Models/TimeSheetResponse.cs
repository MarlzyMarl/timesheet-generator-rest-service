﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TimeSheetAPI.Models
{
    public class TimeSheetResponse
    {
        public string candidateName { get; set; }
        public string clientName { get; set; }
        public string jobTitle { get; set; }
        public string placementStartDate { get; set; }
        public string placementEndDate { get; set; }
        public string placementType { get; set; }
        public Array days { get; set; }
    }
}