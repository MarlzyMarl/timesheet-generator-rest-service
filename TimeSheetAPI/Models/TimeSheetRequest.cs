﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TimeSheetAPI.Models
{
    public class TimeSheetRequest
    {
        public TimeSheetRequest()
        {
            candidateName = "Marl";
            clientName = "myClient";
            jobTitle = "myJobTitle";
            placementStartDate = DateTime.Now.ToString("dd/MM/yyyy");
            placementEndDate = DateTime.Now.ToString("dd/MM/yyyy");
            placementType = enumerators.PlacementType.Weekly;
        }
        public string candidateName { get; set; }
        public string clientName { get; set; }
        public string jobTitle { get; set; }
        public string placementStartDate { get; set; }
        public string placementEndDate { get; set; }
        public enumerators.PlacementType placementType;
    }
}

public class enumerators
{
    public enum PlacementType
    {
        Weekly,
        Monthly
    }
}